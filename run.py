import numpy as np
import matplotlib.pyplot as plt
from keras.models import model_from_json, Sequential
from keras.preprocessing import image
from keras.datasets import mnist
from keras.layers.core import Dense
from keras.utils import np_utils


EPOCHS = 15


def create_model():
    model_ = Sequential()
    model_.add(Dense(800, input_dim=784, activation="relu"))
    model_.add(Dense(10, activation="softmax"))

    model_.compile(loss="categorical_crossentropy", optimizer="adam", metrics=["accuracy"])
    return model_


def save_model():
    model_json = model.to_json()
    with open("model.json", "w") as json_file:
        json_file.write(model_json)

    model.save_weights("model.h5")
    return model


def test_on_single_image(model):
    img_path = input('Enter name of image: ')
    img = image.load_img(img_path, target_size=(28, 28), color_mode="grayscale")
    plt.imshow(img.convert('RGBA'))
    plt.show()

    x = image.img_to_array(img)
    x = x.reshape(1, 784)
    x = 255 - x
    x /= 255

    prediction = model.predict(x)
    print(np.argmax(prediction))


def load_model():
    json_file = open('model.json', 'r')
    loaded_model_json = json_file.read()
    json_file.close()
    loaded_model = model_from_json(loaded_model_json)
    # load weights into new model
    loaded_model.load_weights("model.h5")
    return loaded_model


def prepare_data(data):
    (X_train, Y_train), (X_test, Y_test) = data

    X_train = X_train.reshape(60000, 784)
    X_test = X_test.reshape(10000, 784)

    X_train = X_train.astype('float32') / 255
    X_test = X_test.astype('float32') / 255

    Y_train = np_utils.to_categorical(Y_train, 10)
    Y_test = np_utils.to_categorical(Y_test, 10)

    return (X_train, Y_train), (X_test, Y_test)


ans = input("""Enter option:
1 - train and save model
2 - load and test single image
""")

opt = int(ans)
if opt == 1:
    data = mnist.load_data()
    (X_train, Y_train), (X_test, Y_test) = prepare_data(data=data)

    model = create_model()
    model.fit(X_train, Y_train, batch_size=200, epochs=EPOCHS,  verbose=1)

    save_model()
if opt == 2:
    model = load_model()
    test_on_single_image(model)
